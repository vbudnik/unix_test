
#ifndef FT_TEST_H
# define FT_TEST_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>


char	**ft_get_arguments(char **av, int size);
int		count_arguments(char **ag);
int		check_flag(char *flag);
int		validation_flag(char **arg);
int		ft_b(char *val);
int		ft_c(char *val);
int		ft_d(char *val);
int		ft_e(char *val);
int		ft_f(char *val);
int		ft_g(char *val);
int		ft_h(char *val);
int		ft_k(char *val);
int		ft_n(char *val);
int		ft_p(char *val);
int		ft_r(char *val);
int		ft_s(char *val);
int		ft_t(char *val);
int		ft_u(char *val);
int		ft_w(char *val);
int		ft_x(char *val);
int		ft_z(char *val);
int		ft_L(char *val);
int		ft_S(char *val);
int		ft_equl(char *val1, char *val2);
int		ft_no_equel(char *val1, char *val2);
int		ft_more(char *val1, char *val2);
int		ft_less(char *val1, char *val2);
int		ft_eq(char *val1, char *val2);
int		ft_ne(char *val1, char *val2);
int		ft_gt(char *val1, char *val2);
int		ft_ge(char *val1, char *val2);
int		ft_lt(char *val1, char *val2);
int		ft_le(char *val1, char *val2);
int		ft_nt(char *val, char *val2);
int		ft_ot(char *val, char *val2);
int		ft_ef(char *val, char *val2);
int		ft_check_flag(char *args, char *val, char *val2);
int		size_array(char **args);
int		make_test(char **args, char **param);
int		binary_operatir(char *flag);
int		ft_check_flag_1(char *args, char *val, char *val2);
int		ft_check_flag_2(char *args, char *val, char *val2);
int		ft_check_flag_3(char *args, char *val, char *val2);
int		ft_check_flag_4(char *args, char *val, char *val2);
int		ft_check_flag_5(char *args, char *val, char *val2);
int		ft_check_flag_6(char *args, char *val, char *val2);
int		ft_check_flag_7(char *args, char *val, char *val2);
int		ft_check_flag_8(char *args, char *val, char *val2);
int		ft_test(char **av);

#endif