#include "ft_test.h"

void    free_array(char **array);

int main(int ac, char **av)
{
	ac = ac + 1;
	return (ft_test(av));
}

void    free_array(char **array)
{
    int     i;

    i = 0;
    while(array[i] != NULL)
    {
        free(array[i]);
        array[i] = NULL;
        i++;
    }
    free(array);
    array = NULL;
}

int     ft_test(char **av)
{
    char   **arg;

    if (count_arguments(av) == 1)
        return (-1);
    arg = ft_get_arguments(av, count_arguments(av));
    // зімнити на 1 та 1(2 для теста верший назва файла)
    // if (count_arguments(av) == 1 && size_array(av) == 1)
    if (count_arguments(av) == 2 && size_array(av) == 2)
    {
        free_array(arg);
    	return (0);
    }
    if (validation_flag(av) == 0)
    {
        free_array(arg);
        return(-1);
    }
    if (make_test(av, arg) == 1)
    {
        free_array(arg);
    	return (0);
    }
    return (1);
}

int     binary_operatir(char *flag)
{
	if(strcmp(flag, "-nt") == 0 || strcmp(flag, "-ot") == 0 || strcmp(flag, "-ef") == 0 ||
		strcmp(flag, ">") == 0 || strcmp(flag, "!=") == 0 || strcmp(flag, "=") == 0 ||
		strcmp(flag, "<") == 0 || strcmp(flag, "-eq") == 0)
		return (1);
	return (0);
}

int     make_test(char **args, char **param)
{
	int    i;
	int    j;
	int    ret;

	// i = 0;
	// j = 0;
	// for test with a.out
	i = 1;
	j = 1;
	ret = 0;
	while (args[i] != NULL)
	{
		if (args[i - 1][0] == '-' && args[i][0] == '-')
		{
			printf("test: %s binary operator expected\n", args[i]);
			return (0);	
		}
		if (strcmp(args[i], param[j]) == 0)
		{
			j++;
			i++;
			if(i == size_array(args))
				break ;
		}
		if (binary_operatir(args[i]))
			ret = ft_check_flag(args[i], param[j - 1], param[j]) == 1 ? 1 : -1;
		else
			ret = ft_check_flag(args[i], param[j], NULL) == 1 ? 1 : -1;
		i++;
	}
	return (ret);
}

int     size_array(char **args)
{
	int    i;

	i = 0;
	while(args[i] != NULL)
	{
		i++;
	}
	return (i);
}

int     validation_flag(char **arg)
{
    int     i;

    i = 0;
    while (arg[i] != NULL)
    {
        if (arg[i][0] == '-' || arg[i][0] == '=' || arg[i][0] == '!' ||
            arg[i][0] == '>' || arg[i][0] == '<')
        {
            if (check_flag(arg[i]) == 0)
            {
                printf("test: %s unary operator expected\n", arg[i]);
                return (0);
            }
        }
        i++;
    }
    return (1);
}

char   **ft_get_arguments(char **av, int size)
{
    char   **arg;
    int     i;
    int     j;

    i =0;
    j = 0;
    arg = (char **)malloc(sizeof(char *) * (size + 1));
    while(av[i] != NULL) {
        if (av[i][0] != '-' && av[i][0] != '='
        	&& av[i][0] != '!' && av[i][0] != '>'
        	&& av[i][0] != '<')
        {
            arg[j] = strdup(av[i]);
            j++;
        }
        i++;
    }
    arg[j] = NULL;
    return (arg);
}

int     count_arguments(char **ag) {
    int     i;
    int     count;

    i = 0;
    count= 0;
    while (ag[i] != NULL)
    {
        if (ag[i][0] != '-')
            count++;
        i++;
    }
    return count;
}

int     check_flag(char *flag)
{
    if (strcmp(flag, "=") == 0 || strcmp(flag, "!=") == 0 ||
    	strcmp(flag, ">") == 0 || strcmp(flag, "<") == 0 ||
        strcmp(flag, "-b") == 0 || strcmp(flag, "-c") == 0 ||
        strcmp(flag, "-d") == 0 || strcmp(flag, "-e") == 0 ||
        strcmp(flag, "-f") == 0 || strcmp(flag, "-g") == 0 ||
        strcmp(flag, "-h") == 0 || strcmp(flag, "-k") == 0 ||
        strcmp(flag, "-n") == 0 || strcmp(flag, "-p") == 0 ||
        strcmp(flag, "-r") == 0 || strcmp(flag, "-s") == 0 ||
        strcmp(flag, "-t") == 0 || strcmp(flag, "-u") == 0 ||
        strcmp(flag, "-w") == 0 || strcmp(flag, "-x") == 0 ||
        strcmp(flag, "-z") == 0 || strcmp(flag, "-L") == 0 ||
        strcmp(flag, "-S") == 0 || strcmp(flag, "-le") == 0 ||
        strcmp(flag, "-nt") == 0 || strcmp(flag, "-lt") == 0 ||
        strcmp(flag, "-ot") == 0 || strcmp(flag, "-ef") == 0 ||
        strcmp(flag, "-eq") == 0 || strcmp(flag, "-ne") == 0 ||
        strcmp(flag, "-ge") == 0)
        return (1);
    return (0);
}

/*
** Is block
*/

int     ft_b(char *val)
{
    struct stat     st;

    if(stat(val, &st) == 0 && S_ISBLK(st.st_mode))
        return (1);
    return (0);
}

/*
** Is character device
*/

int     ft_c(char *val)
{
    struct stat     st;

    if(stat(val, &st) == 0 && S_ISCHR(st.st_mode))
        return (1);
    return (0);
}

/*
** Is directory
*/

int     ft_d(char *val)
{
    struct stat     st;

    if(stat(val, &st) == 0 && S_ISDIR(st.st_mode))
        return (1);
    return (0);
}

/*
** Is exist
*/

int     ft_e(char *val)
{
    if(access(val, F_OK) != -1)
        return (1);
    return (0);
}

/*
** Is regular file
*/

int     ft_f(char *val)
{
    struct stat     st;

    if(stat(val, &st) == 0 && S_ISREG(st.st_mode))
        return (1);
    return (0);
}

/*
** Is have group ID
*/

int     ft_g(char *val)
{
    struct stat     st;

    if(stat(val, &st) == 0 && (st.st_gid))
        return (1);
    return (0);
}

/*
** Is symbolyk
*/

int     ft_h(char *val)
{
    struct stat st;

    if(lstat(val, &st) == 0 && (S_ISLNK(st.st_mode)))
        return (1);
    return (0);
}

/*
** Is sticky bit
*/

int     ft_k(char *val)
{
    struct stat     st;

    if(lstat(val, &st) == 0 && (st.st_mode & S_ISVTX))
        return (1);
    return (0);
}

/*
** Is len of string not null
*/

int     ft_n(char *val)
{
    if(strlen(val) != 0)
        return (1);
    return (0);
}

/*
** Is FIFO
*/

int     ft_p(char *val)
{
    struct stat     st;

    if(lstat(val, &st) == 0 && (S_ISFIFO(st.st_mode)))
        return (1);
    return (0);
}

/*
** Is read
*/

int     ft_r(char *val)
{
    struct stat     st;

    if(lstat(val, &st) == 0 && (st.st_mode & S_IREAD))
        return (1);
    return (0);
}

/*
** Is socket
*/

int     ft_s(char *val)
{
    struct stat     st;

    if(lstat(val, &st) == 0 && st.st_size > 0)
        return (1);
    return (0);
}

/*
** open file descriptor referring to a terminal
*/

int     ft_t(char *val)
{
    if(isatty(atoi(val)) == 1)
        return (1);
    return (0);
}

/*
** open user ID of owner
*/

int     ft_u(char *val)
{
    struct stat     st;

    if(stat(val, &st) == 0 && (st.st_uid))
        return (1);
    return (0);
}

/*
** is write
*/

int     ft_w(char *val)
{
    struct stat     st;

    if(stat(val, &st) == 0 && (st.st_mode & S_IWRITE))
    	return (1);
    return (0);
}

/*
** is execute
*/

int     ft_x(char *val)
{
    struct stat     st;

    if(stat(val, &st) == 0 && (st.st_mode & S_IEXEC))
    	return (1);
    return (0);
}

/*
** if len 0f string null
*/

int     ft_z(char *val)
{
    if(strlen(val) == 0)
        return (1);
    return (0);
}

/*
** is symbolyk link
*/

int     ft_L(char *val)
{
    struct stat     st;

    if(lstat(val, &st) == 0 && (S_ISLNK(st.st_mode)))
    	return (1);
    return (0);
}


/*
** is socket
*/

int     ft_S(char *val)
{
    struct stat     st;

    if(stat(val, &st) == 0 && (S_ISSOCK(st.st_mode)))
        return (1);
    return (0);
}


/*
** =
*/

int     ft_equl(char *val1, char *val2)
{
    if(strcmp(val1,val2) == 0)
        return (1);
    return (0);
}

/*
** !=
*/

int     ft_no_equel(char *val1, char *val2)
{
    if(strcmp(val1,val2) != 0)
        return (1);
    return (0);
}

/*
** >
*/

int     ft_more(char *val1, char *val2)
{
    if(strlen(val1) > strlen(val2))
        return (1);
    return (0);
}

/*
** <
*/

int     ft_less(char *val1, char *val2)
{
    if(strlen(val1) < strlen(val2))
        return (1);
    return (0);
}

/*
** equel number
*/

int     ft_eq(char *val1, char *val2)
{
    if(atoi(val1) == atoi(val2))
        return (1);
    return (0);
}

/*
** no equel number
*/

int     ft_ne(char *val1, char *val2)
{
    if(atoi(val1) != atoi(val2))
        return (1);
    return (0);
}

/*
** one number more number two
*/

int     ft_gt(char *val1, char *val2)
{
    if(atoi(val1) > atoi(val2))
        return (1);
    return (0);
}

/*
** one number more number two or equel
*/

int     ft_ge(char *val1, char *val2)
{
    if(atoi(val1) >= atoi(val2))
        return (1);
    return (0);
}

/*
** one number less number two
*/

int     ft_lt(char *val1, char *val2)
{
    if(atoi(val1) < atoi(val2))
        return (1);
    return (0);
}

/*
** one number less number two or equel
*/

int     ft_le(char *val1, char *val2)
{
    if(atoi(val1) <= atoi(val2))
        return (1);
    return (0);
}

/*
** first file more data second
*/

int     ft_nt(char *val, char *val2)
{
	struct stat        st1;
	struct stat        st2;

	if (stat(val, &st1) == 0 && stat(val2, &st2) == 0 &&
		&st1.st_ctime > &st2.st_ctime)
		return (1);
	return (0);
}

/*
** first file less data second
*/

int     ft_ot(char *val, char *val2)
{
	struct stat    st1;
	struct stat    st2;

	if (stat(val, &st1) == 0 && stat(val2, &st2) == 0 &&
		&st1.st_ctime < &st2.st_ctime)
		return (1);
	return (0);
}

/*
** have one inode
*/

int     ft_ef(char *val, char *val2)
{
	struct stat    st1;
	struct stat    st2;

	if (stat(val, &st1) == 0 && stat(val2, &st2) == 0 &&
		&st1.st_ino == &st2.st_ino)
		return (1);
	return (0);
}

int     ft_check_flag_1(char *args, char *val, char *val2)
{
	int        ret;

    ret = 0;
    val2 = val2;
    if (strncmp(args ,"-b", strlen(args)) == 0)
    {
        ret = ft_b(val);
        return (ret);
    }
    if (strncmp(args, "-c", strlen(args))  == 0)
    {
        ret = ft_c(val);
        return (ret);
    }
    if (strncmp(args, "-d", strlen(args))  == 0)
    {
        ret = ft_d(val);
        return (ret);
    }
    if (strncmp(args, "-e", strlen(args))  == 0)
    {
        ret = ft_e(val);
        return (ret);
    }
    return (-1);
}

int     ft_check_flag_2(char *args, char *val, char *val2)
{ 
    int     ret;

    ret = 0;
    val2 = val2;
    if (strncmp(args, "-f", strlen(args))  == 0)
    {
        ret = ft_f(val);
        return (ret);
    }
    if(strncmp(args, "-g", strlen(args))  == 0)
    {
        ret = ft_g(val);
        return (ret);
    }
    if(strncmp(args, "-h", strlen(args))  == 0 )
    {
        ret = ft_h(val);
        return (ret);
    }
    if (strncmp(args, "-k", strlen(args))  == 0)
    {
        ret = ft_k(val);
        return (ret);
    }
    return (-1);
}

int     ft_check_flag_3(char *args, char *val, char *val2)
{ 
    int     ret;

    ret = 0;
    val2 = val2;
    if (strncmp(args, "-n", strlen(args))  == 0)
    {
        ret = ft_n(val);
        return (ret);
    }
    if (strncmp(args, "-p", strlen(args)) == 0)
    {
        ret = ft_p(val);
        return (ret);
    }
    if (strncmp(args, "-r", strlen(args)) == 0)
    {
        ret = ft_r(val);
        return (ret);
    }
    if (strncmp(args, "-s", strlen(args))  == 0)
    {
        ret = ft_s(val);
        return (ret);
    }
    return (-1);
}

int     ft_check_flag_4(char *args, char *val, char *val2)
{ 
    int     ret;

    ret = 0;
    val2 = val2;
    if (strncmp(args, "-t", strlen(args)) == 0)
    {
        ret = ft_t(val);
        return (ret);
    }
    if (strncmp(args, "-u", strlen(args)) == 0)
    {
        ret = ft_u(val);
        return (ret);
    }
    if (strncmp(args, "-w", strlen(args)) == 0)
    {
        ret = ft_w(val);
        return (ret);
    }
    if(strncmp(args, "-x", strlen(args)) == 0)
    {
        ret = ft_x(val);
        return (ret);
    }
    return (-1);
}

int     ft_check_flag_5(char *args, char *val, char *val2)
{ 
    int     ret;

    ret = 0;
    if (strncmp(args, "-z", strlen(args)) == 0)
    {
        ret = ft_z(val);
        return (ret);
    }
    if (strncmp(args, "-L", strlen(args)) == 0)
    {
        ret = ft_L(val);
        return (ret);
    }
    if (strncmp(args, "-S", strlen(args)) == 0)
    {
        ret = ft_S(val);
        return (ret);
    }
    if (strncmp(args, "-nt", strlen(args)) == 0)
    {
    	ret = ft_nt(val, val2);
    	return (ret);
    }
    return (-1);
}

int     ft_check_flag_6(char *args, char *val, char *val2)
{ 
    int     ret;

    ret = 0;
    if (strncmp(args, "-ot", strlen(args)) == 0)
    {
    	ret = ft_ot(val, val2);
    	return (ret);
    }
    if (strncmp(args, "-ef", strlen(args)) == 0)
    {
    	ret = ft_ef(val, val2);
    	return (ret);
    }
    if (strncmp(args, "=", strlen(args)) == 0)
    {
        ret = ft_equl(val, val2);
        return (ret);
    }
    if (strncmp(args, "!=", strlen(args)) == 0)
    {
        ret = ft_no_equel(val, val2);
        return (ret);
    }
    return (-1);
}

int     ft_check_flag_7(char *args, char *val, char *val2)
{ 
    int     ret;

    ret = 0;
    if (strncmp(args, ">", strlen(args)) == 0)
    {
        ret = ft_more(val, val2);
        return (ret);
    }
    if (strncmp(args, "<", strlen(args)) == 0)
    {
        ret = ft_less(val, val2);
        return (ret);
    }
    if (strncmp(args, "-eq", strlen(args)) == 0)
    {
        ret = ft_eq(val, val2);
        return (ret);
    }
    if (strncmp(args, "-ne", strlen(args)) == 0)
    {
        ret = ft_ne(val, val2);
        return (ret);
    }
    return (-1);
}

int     ft_check_flag_8(char *args, char *val, char *val2)
{ 
    int     ret;

    ret = 0;
    if(strncmp(args, "-gt", strlen(args)) == 0)
    {
        ret = ft_gt(val, val2);
        return (ret);
    }
    if (strncmp(args, "-ge", strlen(args)) == 0)
    {
        ret = ft_ge(val, val2);
        return (ret);
    }
    if (strncmp(args, "-lt", strlen(args)) == 0)
    {
        ret = ft_lt(val, val2);
        return (ret);
    }
    if (strncmp(args, "-le", strlen(args)) == 0)
    {
        ret = ft_le(val, val2);
        return (ret);
    }
    return (-1);
}

int     ft_check_flag(char *args, char *val, char *val2)
{
    int     ret;

    ret = 0;
    if((ret = ft_check_flag_1(args, val, val2)) != -1)
    	return (ret);
    if((ret = ft_check_flag_2(args, val, val2)) != -1)
    	return (ret);
    if((ret = ft_check_flag_3(args, val, val2)) != -1)
    	return (ret);
    if((ret = ft_check_flag_4(args, val, val2)) != -1)
    	return (ret);
    if((ret = ft_check_flag_5(args, val, val2)) != -1)
    	return (ret);
    if((ret = ft_check_flag_6(args, val, val2)) != -1)
    	return (ret);
    if((ret = ft_check_flag_7(args, val, val2)) != -1)
    	return (ret);
    if((ret = ft_check_flag_8(args, val, val2)) != -1)
    	return (ret);
    return (-1);
}